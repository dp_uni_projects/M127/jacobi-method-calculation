"""
Kalhspera
"""
import numpy as np
import plotly.express as px
import plotly.graph_objects as go
import pandas as pd

# import plotly.graph_objects as go

data = pd.read_csv(
    "matrix.txt",
    names=["x", "y", "z"],
    dtype={"x": np.double, "y": np.double, "z": np.double},
)
data = data.pivot(index="y", columns="x", values="z")
fig = go.Figure(
    go.Surface(z=data.values.tolist(), x=data.columns.tolist(), y=data.index.tolist())
)                               # Can also be changed to go.Heatmap for 2D plot
fig.update_layout(autosize=True)
fig["layout"]["yaxis"]["scaleanchor"] = "x"

fig.show()
