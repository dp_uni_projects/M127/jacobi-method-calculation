#!/bin/bash

# JobName #
#PBS -N jacobi_hybrid

#Which Queue to use #
#PBS -q N10C80

# Only this job uses the chosen nodes
#PBS -l place=excl

# Stdout Output File, if not provided a <JobName>.o<JobID> will be created #
#PBS -o ./results/jacobi_hybrid.out

# Stderr Output File, if not provided a <JobName>.e<JobID> will be created #
#PBS -e ./results/jacobi_hybrid.err

# Max Wall time, Example 1 Minute #
#PBS -l walltime=00:20:00

# How many nodes and tasks per node
#PBS -l select=1:ncpus=8:mpiprocs=1:ompthreads=2:mem=16400000kb

#Change Working directory to SUBMIT directory
cd $PBS_O_WORKDIR

# Run executable #
mpirun --bind-to none -np 1 bin/jacobi_hybrid_mpip.x < input
