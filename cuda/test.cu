#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include <time.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include "device_launch_parameters.h"

#define SIZE_X		   10
#define SIZE_Y		   10
#define TOTAL_ELEMENTS (SIZE_X * SIZE_Y)

using namespace std;
__global__ void cuda_hello()
{
	int blockId = (gridDim.x * blockIdx.y) + blockIdx.x;
	int threadId = (blockId * blockDim.x) + threadIdx.x;

	int x = blockIdx.x * blockDim.x + threadIdx.x; // Assuming 1D blocks
	int y = blockIdx.y;							   // 1D blocks in 2D grid
	printf("%d: %d x %d\n", threadId, x, y);
	// printf("%2d\t%2d\t%2d\t%2d\n", threadId, blockIdx.x, blockIdx.y, threadIdx.x);
	// if(threadId == 0)
	// {
	// 	printf("Grid dims %2d x %2d\n", gridDim.x, gridDim.y);
	// }
}

int main(void)
{

	cudaError_t cudaerr;
	const int	BLOCK_SIZE = 3;
	int			grid_size_x = (int)ceil(((float)SIZE_X / (float)BLOCK_SIZE));
	dim3		dimBl(BLOCK_SIZE);
	dim3		dimGr(grid_size_x, SIZE_Y);

	printf("Block size %d, Grid size: rows %2d, cols %2d\n", BLOCK_SIZE, SIZE_Y, grid_size_x);

	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	int *cpu_array;
	int *gpu_array;

	cpu_array = (int *)calloc(TOTAL_ELEMENTS, sizeof(int));

	cudaerr = cudaMalloc((void **)&gpu_array, SIZE_X * SIZE_Y * sizeof(int));
	if(cudaerr != cudaSuccess)
	{
		printf("Failed to allocate %ld Mbytes in GPU\n", (TOTAL_ELEMENTS * sizeof(int)) >> 20);
		fprintf(stderr, "GPUassert: %d\n", cudaerr);
		return cudaerr;
	}
	printf("Allocated %ld Mbytes in GPU\n", (TOTAL_ELEMENTS * sizeof(int)) >> 20);
	cudaerr = cudaFree((void **)&gpu_array);
	// printf("id\tb.x\tb.y\tt.x\n");
	// printf("_____________________\n");
	cudaEventRecord(start);
	cuda_hello<<<dimGr, dimBl>>>();
	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	float milliseconds = 0;
	cudaEventElapsedTime(&milliseconds, start, stop);

	cudaerr = cudaDeviceSynchronize();
	if(cudaerr != cudaSuccess)
	{
		printf("kernel launch failed with error \"%s\".\n", cudaGetErrorString(cudaerr));
	}

	free(cpu_array);
	printf("Runtime=%06.4f ms\n", milliseconds);

	return 0;
}
