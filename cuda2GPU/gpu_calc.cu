#include <lcutil.h>
#include <timestamp.h>
#include <omp.h>

#define BLOCK_SIZE 512

__global__ void calc(double *u,
					 double *u_old,
					 int	 n,
					 int	 m,
					 double	 alpha,
					 double	 relax,
					 double	 tol,
					 double *error_array_device,
					 int	 device)
{

	// Solve in [-1, 1] x [-1, 1]
	double xLeft = -1.0, xRight = 1.0;
	double yBottom = -1.0, yUp = 1.0;

	double deltaX = (1.0 - (-1.0)) / (n - 1);
	double deltaY = (1.0 - (-1.0)) / (m * 2 - 1);

	yUp = 1.0 - device * (gridDim.y - 1) * deltaY;
	yBottom = yUp - (gridDim.y - 1) * deltaY;

	int iterationCount = 0;

#define SRC(XX, YY) u_old[(YY) * (n + 2) + (XX)] // prosbash ana grammh, aplo
#define DST(XX, YY) u[(YY) * (n + 2) + (XX)]
	int	   x, y;
	double fX, fY;

	double updateVal;
	//  double f;
	// Coefficients
	double cx = 1.0 / (deltaX * deltaX);
	double cy = 1.0 / (deltaY * deltaY);
	double cc = -2.0 * cx - 2.0 * cy - alpha;
	// double			  local_error = 0.0;
	// __shared__ double total_error = HUGE_VAL; // to total error to grafoun ola mazi
	// __shared__ double error = 0.0;

	int blockId = (gridDim.x * blockIdx.y) + blockIdx.x;
	int threadId = (blockId * blockDim.x) + threadIdx.x;
	// if(threadId == 0)
	// {
	// 	printf("%d: dY=%.2f, yUp=%.2f, yBottom=%.2f\n", device, deltaY, yUp, yBottom);
	// }

	// briskw se poio block eimai orizontia * posa threads exei kathe block = poses sthles exw
	// aristera mou + poio thread eimai sthn orizontai diata3h tou block
	x = (blockIdx.x * blockDim.x + threadIdx.x) + 1;
	y = blockIdx.y + 1;

	if(x < (n + 2 - 1)) // if current thread is within the array
	{
		int array_write_idx = (y - 1) * n + (x - 1);
		// printf("Thread %5d: Write at %5d, Coords: %5d, %5d\n", threadId, array_write_idx, x, y);
		//      error_array_device[threadId] = 0.0;
		fY = yUp - (y - 1) * deltaY;
		updateVal = ((SRC(x - 1, y) + SRC(x + 1, y)) * cx + (SRC(x, y - 1) + SRC(x, y + 1)) * cy
					 + SRC(x, y) * cc
					 - (-alpha * (1.0 - (xLeft + (x - 1) * deltaX) * (xLeft + (x - 1) * deltaX))
							* (1.0 - fY * fY)
						- 2.0 * (1.0 - (xLeft + (x - 1) * deltaX) * (xLeft + (x - 1) * deltaX))
						- 2.0 * (1.0 - fY * fY)))
					/ cc;

		DST(x, y) = SRC(x, y) - relax * updateVal;

		error_array_device[array_write_idx] = updateVal * updateVal;
	}
}

__global__ void reduce_sum(double *array, int array_size, double *total_error)
{
	int	   idx = threadIdx.x;
	double sum = 0;
	for(int i = idx; i < array_size; i += blockDim.x)
	{
		sum += array[i];
	}
	__shared__ double r[1024];
	r[idx] = sum; // Store current thread's local sum
	__syncthreads();
	for(int size = blockDim.x / 2; size > 0; size /= 2) // uniform
	{
		if(idx < size)
		{
			r[idx] += r[idx + size];
		}
		__syncthreads();
	}
	if(idx == 0)
	{
		*total_error = r[0];
	}
}

extern "C" float jacobiGPU(double *h_u,
						   double *h_u_old,
						   int	   n,
						   int	   m,
						   double  alpha,
						   double  relax,
						   double  tol,
						   int	   mits,
						   int	   thread_id,
						   double *shared_halo,
						   double *h_total_error)
{
	cudaError_t err;
	int			allocCount = (n + 2) * (m + 2);
	double *	d_u;
	double *	d_u_old;
	printf("allocCount(GPU)=%d (%5d rows x %5d cols)\n", allocCount, n + 2, m + 2);

	err = cudaSetDevice(thread_id);
	if(err != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s\n", err);
		return err;
	}

	err = cudaMalloc((void **)&d_u, allocCount * sizeof(double));
	if(err != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s\n", err);
		return err;
	}

	err = cudaMalloc((void **)&d_u_old, allocCount * sizeof(double));
	if(err != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s\n", err);
		return err;
	}
	printf("Thread %d: will send to GPU %d bytes (%d KB)\n",
		   thread_id,
		   allocCount * sizeof(double),
		   allocCount * sizeof(double) >> 10);

	// Copy data to device memory
	// err = cudaMemcpy(d_u, h_u, sizeof(double) * allocCount, cudaMemcpyHostToDevice);
	// if(err != cudaSuccess)
	// {
	// 	fprintf(stderr, "GPUassert: %s\n", err);
	// 	return err;
	// }

	// err = cudaMemcpy(d_u_old,
	// 				 h_u_old,
	// 				 sizeof(double) * allocCount,
	// 				 cudaMemcpyHostToDevice); // pithanh kathusterhsh

	// if(err != cudaSuccess)
	// {
	// 	fprintf(stderr, "GPUassert: %s\n", err);
	// 	return err;
	// }

	// Initialize the two device arrays to zeros
	err = cudaMemset((void *)d_u, 0, sizeof(double) * allocCount);
	err = cudaMemset((void *)d_u_old, 0, sizeof(double) * allocCount);

	// return 0.0;
	dim3 dimBl(BLOCK_SIZE); // 1D block

	// sto x exw n+2 sthles, ston y exw n+2 grammes, ara orizontai kanw
	// stoixeia/threads, katheta kanw tosa block osa stoixeia exw, ta threads ta
	// pairnw ana grammh
	dim3 dimGr(FRACTION_CEILING(n, BLOCK_SIZE), m);

	double *d_error_array;

	int error_array_size = (n * m);
	err = cudaMalloc((void **)&d_error_array, sizeof(double) * error_array_size);
	if(err != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s\n", err);
		return err;
	}

	// Where total error after reduce sum
	// will be stored in
	double *d_total_error;

	err = cudaMalloc((void **)&d_total_error, sizeof(double));
	if(err != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s\n", err);
		return err;
	}

	// Initialize error to a huge value
	*h_total_error = HUGE_VAL;

	err = cudaMemcpy(d_total_error,
					 h_total_error,
					 sizeof(double),
					 cudaMemcpyHostToDevice); // pithanh kathusterhsh
	if(err != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s\n", err);
		return err;
	}

	// printf("Hi I am thread id %d\n", omp_get_thread_num());

#pragma omp barrier
	timestamp t_start;
	t_start = getTimestamp();
	int iterationCount = 0;

#pragma omp reduction(+ : h_total_error)
	while(iterationCount < mits && h_total_error[0] > tol)
	{
#pragma omp barrier
		// Send the required halo points to each GPU
		if(omp_get_thread_num() == 0) // GPU 0
		{
			err = cudaMemcpy((d_u_old + (n + 2) * (m + 1)),
							 (shared_halo + n + 2),
							 (n + 2) * sizeof(double),
							 cudaMemcpyHostToDevice);
		}
		else if(omp_get_thread_num() == 1) // GPU 1
		{
			err = cudaMemcpy(
				d_u_old, shared_halo, (n + 2) * sizeof(double), cudaMemcpyHostToDevice);
		}
#pragma omp barrier
		// kernel call
		calc<<<dimGr, dimBl>>>(
			d_u, d_u_old, n, m, alpha, relax, tol, d_error_array, omp_get_thread_num());

		err = cudaDeviceSynchronize();
		if(err != cudaSuccess)
		{
			fprintf(stderr, "GPUassert: %s\n", err);
			// return err;
		}

		// Call reduction kernel
		reduce_sum<<<1, 1024>>>(d_error_array, error_array_size, d_total_error);

		err = cudaDeviceSynchronize(); // sygxronismos anamonh na teleiwsoun oles oi douleies
		if(err != cudaSuccess)
		{
			fprintf(stderr, "GPUassert: %s\n", err);
			// return err;
		}

		// Copy total error from device to host
		err = cudaMemcpy(
			&h_total_error[thread_id], d_total_error, sizeof(double), cudaMemcpyDeviceToHost);

#pragma omp barrier
#pragma omp single
		{
			h_total_error[0] += h_total_error[1];
		} // implicit barrier

		// printf("Thread %d, h_total_error=%g\n", omp_get_thread_num(), h_total_error[0]);

#pragma omp barrier

		if(omp_get_thread_num() == 0)
		{
			err = cudaMemcpy(shared_halo,
							 (d_u + ((n + 2) * (m))),
							 (n + 2) * sizeof(double),
							 cudaMemcpyDeviceToHost);
		}
		else if(omp_get_thread_num() == 1)
		{
			err = cudaMemcpy((shared_halo + n + 2),
							 (d_u + (n + 2)),
							 (n + 2) * sizeof(double),
							 cudaMemcpyDeviceToHost);
		}

		// Swap the pointers
		double *temp = d_u_old;
		d_u_old = d_u;
		d_u = temp;

		iterationCount++;
#pragma omp barrier
	} // while loop end
	float msecs = getElapsedtime(t_start);
	printf("Run time %.5f s\n", msecs / 1000);

	if(err != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s\n", err);
		return err;
	}

#pragma omp single
	{
		printf("Error %g\n", h_total_error[0]);
		printf("Run %d iterations\n", iterationCount);
	}

	err = cudaGetLastError();
	if(err != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s\n", err);
		return err;
	}

	err = cudaDeviceSynchronize(); // sygxronismos anamonh na teleiwsoun oles oi douleies
	if(err != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s\n", err);
		return err;
	}

	// Copy results back to host memory
	err = cudaMemcpy(h_u_old,
					 d_u_old + (thread_id * (n + 2)),
					 sizeof(double) * (allocCount - (n + 2)),
					 cudaMemcpyDeviceToHost);
	if(err != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s\n", err);
		return err;
	}

	// Free memory
	err = cudaFree(d_u);
	if(err != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s\n", err);
		return err;
	}
	err = cudaFree(d_u_old);
	if(err != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s\n", err);
		return err;
	}

	return msecs;
}
