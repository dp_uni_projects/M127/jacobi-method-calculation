# Jacobi method calculation
## Method
Program to solve a finite difference
discretization of the screened Poisson equation:

`(d2/dx2)u + (d2/dy2)u - alpha u = f`

with zero Dirichlet boundary condition using the iterative
Jacobi method with overrelaxation.

RHS (source) function:

`f(x,y) = -alpha*(1-x^2)(1-y^2)-2*[(1-x^2)+(1-y^2)]`

Analytical solution to the PDE:

`u(x,y) = (1-x^2)(1-y^2)`

Plot of the expected result (20x20 problem size):

![Expected solution](https://gitlab.com/dp_uni_projects/M127/jacobi-method-calculation/-/raw/master/doc/report/images/expected_20x20.png "Solution 20x20")


## Installing Requirements
You will need a Python3 installation to use the plotting script.
1. `python3 -m venv plot`
2. `plot/bin/pip install -r requirements.txt`
3. `plot/bin/python --version`
## Compilation
### With the Makefile
#### Jacobi (Serial)
`make jacobi_serial`
#### Jacobi (MPI)
`make jacobi_mpi`
#### Jacobi (Hybrid)
`make jacobi_hybrid`
## Execution
### Editing the program's parameters
Simply edit the first line of the `input` file. It specifies
the problem's dimensions (X x Y), separated with a comma.
### Manually
#### MPI
##### Run
`mpirun -np <num_procs> bin/jacobi_mpi.x < input`
##### Run and plot
- Compile with `SAVE_TO_FILE` defined
- `rm matrix.txt & mpirun -np <num_procs> bin/jacobi_mpi.x < input && plot/bin/python 2dplot.py`
#### Hybrid
##### Run
- Run `export OMP_NUM_THREADS=<num of threads>`
`mpirun -np <num_procs> bin/jacobi_hybrid.x < input`
### PBS Script
