/************************************************************
 * Program to solve a finite difference
 * discretization of the screened Poisson equation:
 * (d2/dx2)u + (d2/dy2)u - alpha u = f
 * with zero Dirichlet boundary condition using the iterative
 * Jacobi method with overrelaxation.
 *
 * RHS (source) function
 *   f(x,y) = -alpha*(1-x^2)(1-y^2)-2*[(1-x^2)+(1-y^2)]
 *
 * Analytical solution to the PDE
 *   u(x,y) = (1-x^2)(1-y^2)
 *
 * Current Version: Christian Iwainsky, RWTH Aachen University
 * MPI C Version: Christian Terboven, RWTH Aachen University, 2006
 * MPI Fortran Version: Dieter an Mey, RWTH Aachen University, 1999 - 2005
 * Modified: Sanjiv Shah,        Kuck and Associates, Inc. (KAI), 1998
 * Author:   Joseph Robicheaux,  Kuck and Associates, Inc. (KAI), 1998
 *
 * Unless READ_INPUT is defined, a meaningful input dataset is used (CT).
 *
 * Input : n     - grid dimension in x direction
 *         m     - grid dimension in y direction
 *         alpha - constant (always greater than 0.0)
 *         tol   - error tolerance for the iterative solver
 *         relax - Successice Overrelaxation parameter
 *         mits  - maximum iterations for the iterative solver
 *
 * On output
 *       : u(n,m)       - Dependent variable (solution)
 *       : f(n,m,alpha) - Right hand side function
 *
 *************************************************************/

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <mpi.h>
#include <omp.h>

// #define DEBUG
//#define SAVE_TO_FILE
#define NORTH 0
#define EAST  1
#define SOUTH 2
#define WEST  3

/**********************************************************
 * Checks the error between numerical and exact solutions
 **********************************************************/
double checkSolution(double	 xStart,
					 double	 yStart,
					 int	 maxXCount,
					 int	 maxYCount,
					 double *u,
					 double	 deltaX,
					 double	 deltaY,
					 double	 alpha)
{
#define U(XX, YY) u[(YY)*maxXCount + (XX)]
	int	   x, y;
	double fX, fY;
	double localError, error = 0.0;

	for(y = 1; y < (maxYCount - 1); y++)
	{
		fY = yStart + (y - 1) * deltaY;
		for(x = 1; x < (maxXCount - 1); x++)
		{
			fX = xStart + (x - 1) * deltaX;
			localError = U(x, y) - (1.0 - fX * fX) * (1.0 - fY * fY);
			error += localError * localError;
		}
	}
	return sqrt(error) / ((maxXCount - 2) * (maxYCount - 2));
}

int main(int argc, char **argv)
{
	int		n, m, mits; // m: rows, n: columns
	double	alpha, tol, relax;
	double	maxAcceptableError;
	double	local_error;
	double *u, *u_old, *tmp;
	int		allocCount;
	int		iterationCount, maxIterationCount;
	double	t1, t2;
	double	TEMP_COL[m];

	MPI_Init(NULL, NULL);

	// Get the number of processes
	int world_size;
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);

	// Get the rank of the process
	int world_rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

	// Scan parameters from input if we're process 0
	if(world_rank == 0)
	{
		//    printf("Input n,m - grid dimension in x,y direction:\n");
		scanf("%d,%d", &n, &m);
		//    printf("Input alpha - Helmholtz constant:\n");
		scanf("%lf", &alpha);
		//    printf("Input relax - successive over-relaxation parameter:\n");
		scanf("%lf", &relax);
		//    printf("Input tol - error tolerance for the iterrative solver:\n");
		scanf("%lf", &tol);
		//    printf("Input mits - maximum solver iterations:\n");
		scanf("%d", &mits);
	}

	/********** Parameters Datatype creation begin **********/
	// Build a datatype to broadcast parameters to all processes
	// n, m, alpha, relax, tol, mits
	MPI_Datatype parameters_struct;
	MPI_Datatype array_of_types[6]
		= { MPI_INT, MPI_INT, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_INT };
	int datatype_blocklengths[6]
		= { 1, 1, 1, 1, 1, 1 }; // datatype contains one of each type specified above

	MPI_Aint n_addr, m_addr, alpha_addr, relax_addr, tol_addr, mits_addr;
	MPI_Aint datatype_displacements[6] = { 0 };

	MPI_Get_address(&n, &n_addr);
	MPI_Get_address(&m, &m_addr);
	MPI_Get_address(&alpha, &alpha_addr);
	MPI_Get_address(&relax, &relax_addr);
	MPI_Get_address(&tol, &tol_addr);
	MPI_Get_address(&mits, &mits_addr);

	datatype_displacements[1] = m_addr - n_addr;
	datatype_displacements[2] = alpha_addr - n_addr;
	datatype_displacements[3] = relax_addr - n_addr;
	datatype_displacements[4] = tol_addr - n_addr;
	datatype_displacements[5] = mits_addr - n_addr;

	MPI_Type_create_struct(
		6, datatype_blocklengths, datatype_displacements, array_of_types, &parameters_struct);
	MPI_Type_commit(&parameters_struct);
	/********** Parameters Datatype creation end **********/

	/********** Broadcast parameters start **********/
	// Broadcast parameters read from input
	MPI_Bcast(&n, 1, parameters_struct, 0, MPI_COMM_WORLD);
	MPI_Type_free(&parameters_struct);

// Verify that parameters were received by all processes
#ifdef DEBUG
	printf("Process %d: %d x %d, a=%g, relax=%g, tol=%g, mits=%d\n",
		   world_rank,
		   n,
		   m,
		   alpha,
		   relax,
		   tol,
		   mits);
#endif
	/********** Broadcast parameters end **********/

	maxIterationCount = mits;
	maxAcceptableError = tol;

	/********** Cartesian topology creation start **********/
	MPI_Comm cart_comm;
	int		 cart_dim_size[2] = { 0, 0 };
	int		 periods[2] = { 0, 0 };
	MPI_Dims_create(world_size, 2, cart_dim_size); // Calculate dims size

	// 2D, dimensions calculated by MPI, no periodicity, reorder
	MPI_Cart_create(MPI_COMM_WORLD, 2, cart_dim_size, periods, 1, &cart_comm);
	int coords[2]; // Current process' coordinates in the cartesian grid
	MPI_Cart_coords(cart_comm, world_rank, 2, coords);
#ifdef DEBUG
	printf("Process %d, coords: %d, %d\n", world_rank, coords[0], coords[1]);
	printf("Cart Dimsize %d: Cart rows = %d, Cart cols = %d\n",
		   world_rank,
		   cart_dim_size[0],
		   cart_dim_size[1]);

#endif

	/********* Cartesian topology creation end **********/

	// Solve in [-1, 1] x [-1, 1]
	// Put this here to calculate deltaX and deltaY
	// before updating n and m
	double xLeft = -1.0, xRight = 1.0;
	double yBottom = -1.0, yUp = 1.0;

	// deltaX, deltaY are the same for all processes
	double deltaX = (xRight - xLeft) / (n - 1);
	double deltaY = (yUp - yBottom) / (m - 1);

	/********* Memory allocation start *********/
	// Calculate local dimensions
	// Typecasting to int is not really safe, unless problem size
	// dimensions are exactly divisible by cart dimensions calculated
	// by MPI
	m = (int)(m / cart_dim_size[0]);
	n = (int)(n / cart_dim_size[1]);
#ifdef DEBUG
	printf("Problem size %d: rows = %d, cols = %d\n", world_rank, ((m)), ((n)));
	printf("Process %d top-left coord: X = %d, Y = %d\n",
		   world_rank,
		   ((coords[1] * n)),
		   ((coords[0] * m)));
#endif

	// printf("Process %d array size: %04d x %04d\n", world_rank, m, n);
	allocCount = (n + 2) * (m + 2);

	// Those two calls also zero the boundary elements
	u = (double *)calloc(allocCount, sizeof(double)); // reverse order
	u_old = (double *)calloc(allocCount, sizeof(double));

	// printf("allocCount=%d u=%p u_old=%p\n", allocCount, u, u_old);

	if(u == NULL || u_old == NULL)
	{
		printf("Not enough memory for two %ix%i matrices\n", n + 2, m + 2);
		MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
		// exit(1);
	}
	/********* Memory allocation end *********/

	// Local grid edge values
	double xL = xLeft + coords[1] * n * deltaX;
	double xR = xL + (n - 1) * deltaX;
	double yU = yUp - coords[0] * m * deltaY;
	double yB = yU - (m - 1) * deltaY;
#ifdef DEBUG
	printf("DeltaX %f, DeltaY %f\n", deltaX, deltaY);
	printf("Process %d, grid: xL=%f, xR=%f, yU=%f, yB=%f\n", world_rank, xL, xR, yU, yB);
#endif
	xLeft = xL;
	xRight = xR;
	yUp = yU;
	yBottom = yB;

	/********* Col Datatype creation start *********/
	// https://www.rookiehpc.com/mpi/docs/mpi_type_vector.php
	MPI_Datatype column_type;

	// Create a column type which contains m * 1 * MPI_DOUBLE, with stride = n+2
	MPI_Type_vector(
		m, 1, n + 2, MPI_DOUBLE, &column_type); // TODO -- adjust to subprocess array size
	MPI_Type_commit(&column_type);
	/********* Col Datatype creation end *********/

	/********* Neighbour calculation start *********/
	int neighbours[4];
	MPI_Cart_shift(cart_comm, 0, 1, &neighbours[NORTH], &neighbours[SOUTH]);
	MPI_Cart_shift(cart_comm, 1, 1, &neighbours[WEST], &neighbours[EAST]);
	// Verify neighbours
#ifdef DEBUG
	if(world_rank == 0)
	{
		printf("Neighbours(%d) NORTH:%d, EAST:%d, SOUTH:%d, WEST:%d\n",
			   world_rank,
			   neighbours[NORTH],
			   neighbours[EAST],
			   neighbours[SOUTH],
			   neighbours[WEST]);
	}
#endif

	// TODO -- persistent communication????
	/********* Neighbour calculation end *********/

	/********* Jacobi calculation start *********/
	/*************************************************************
	 * Performs one iteration of the Jacobi method and computes
	 * the residual value.
	 *
	 * NOTE: u(0,*), u(maxXCount-1,*), u(*,0) and u(*,maxYCount-1)
	 * are BOUNDARIES and therefore not part of the solution.
	 *************************************************************/
#define SRC(XX, YY) u_old[(YY) * (n + 2) + (XX)]
#define DST(XX, YY) u[(YY) * (n + 2) + (XX)]
	int	   x, y;
	double fX, fY;
	double error = 0.0;
	double updateVal;
	double f;
	// Coefficients, calculated before loop
	double cx = 1.0 / (deltaX * deltaX);
	double cy = 1.0 / (deltaY * deltaY);
	double cc = -2.0 * cx - 2.0 * cy - alpha;
	double total_error = HUGE_VAL;
	// MPI_Request requests[4]; // One request for each neighbour

	/********* Start Persistent communication configuration *********/
	MPI_Request send_requests_1[4];
	MPI_Request send_requests_2[4];
	MPI_Request receive_requests_1[4];
	MPI_Request receive_requests_2[4];
	MPI_Status	send_statuses_1[4];
	MPI_Status	send_statuses_2[4];
	MPI_Status	receive_statuses_1[4];
	MPI_Status	receive_statuses_2[4];

	// Persistent communication -- receive to memory location originally allocated as u_old
	MPI_Recv_init(
		&u_old[1], n, MPI_DOUBLE, neighbours[NORTH], 0, cart_comm, &receive_requests_1[NORTH]);
	MPI_Recv_init(&u_old[(n + 2) * (m + 1) + 1],
				  n,
				  MPI_DOUBLE,
				  neighbours[SOUTH],
				  0,
				  cart_comm,
				  &receive_requests_1[SOUTH]);
	MPI_Recv_init(&u_old[2 * n + 3],
				  1,
				  column_type,
				  neighbours[EAST],
				  0,
				  cart_comm,
				  &receive_requests_1[EAST]);
	MPI_Recv_init(
		&u_old[n + 2], 1, column_type, neighbours[WEST], 0, cart_comm, &receive_requests_1[WEST]);

	// Persistent communication -- receive to memory location originally allocated as u
	MPI_Recv_init(
		&u[1], n, MPI_DOUBLE, neighbours[NORTH], 0, cart_comm, &receive_requests_2[NORTH]);
	MPI_Recv_init(&u[(n + 2) * (m + 1) + 1],
				  n,
				  MPI_DOUBLE,
				  neighbours[SOUTH],
				  0,
				  cart_comm,
				  &receive_requests_2[SOUTH]);
	MPI_Recv_init(
		&u[2 * n + 3], 1, column_type, neighbours[EAST], 0, cart_comm, &receive_requests_2[EAST]);
	MPI_Recv_init(
		&u[n + 2], 1, column_type, neighbours[WEST], 0, cart_comm, &receive_requests_2[WEST]);

	// Persistent communication -- send from memory location originally allocated as u_old
	MPI_Send_init(&u_old[(n + 2) + 1],
				  n,
				  MPI_DOUBLE,
				  neighbours[NORTH],
				  0,
				  cart_comm,
				  &send_requests_1[NORTH]);
	MPI_Send_init(&u_old[(n + 2) * m + 1],
				  n,
				  MPI_DOUBLE,
				  neighbours[SOUTH],
				  0,
				  cart_comm,
				  &send_requests_1[SOUTH]);
	MPI_Send_init(
		&u_old[2 * n + 2], 1, column_type, neighbours[EAST], 0, cart_comm, &send_requests_1[EAST]);
	MPI_Send_init(
		&u_old[n + 3], 1, column_type, neighbours[WEST], 0, cart_comm, &send_requests_1[WEST]);

	// Persistent communication -- send from memory location originally allocated as u
	MPI_Send_init(
		&u[(n + 2) + 1], n, MPI_DOUBLE, neighbours[NORTH], 0, cart_comm, &send_requests_2[NORTH]);
	MPI_Send_init(&u[(n + 2) * m + 1],
				  n,
				  MPI_DOUBLE,
				  neighbours[SOUTH],
				  0,
				  cart_comm,
				  &send_requests_2[SOUTH]);
	MPI_Send_init(
		&u[2 * n + 2], 1, column_type, neighbours[EAST], 0, cart_comm, &send_requests_2[EAST]);
	MPI_Send_init(
		&u[n + 3], 1, column_type, neighbours[WEST], 0, cart_comm, &send_requests_2[WEST]);

	/********* End Persistent communication configuration *********/

	iterationCount = 0;
	local_error = HUGE_VAL;
	clock_t start = clock(), diff;
	MPI_Pcontrol(1);

	MPI_Barrier(MPI_COMM_WORLD);
	t1 = MPI_Wtime(); // Program start time

/* Iterate as long as it takes to meet the convergence criterion */
#pragma omp parallel private(iterationCount, y)
	while(iterationCount < maxIterationCount && total_error > maxAcceptableError)
	{
		/********** Inlined one_jacobi_iteration *********/
		// printf("mpirank=%2d Trank=%2d (total=%2d)\n",
		// 	   world_rank,
		// 	   omp_get_thread_num(),
		// 	   omp_get_num_threads());

#pragma omp master
		// One thread starts communication with other MPI processes
		{
			// Persistent communication is configured only once, before the calculation
			// starts. This means that the pointers passed as arguments to the _init functions
			// always point to the same array (in memory), regardless of the pointer
			// swaps that we do later on. This means that we have to alterante manually
			// between the two requests, which refer to the two u and u_old arrays.
			if(iterationCount % 2 == 0)
			{
				MPI_Startall(4, send_requests_1);
				MPI_Startall(4, receive_requests_1);
			}
			else
			{
				MPI_Startall(4, send_requests_2);
				MPI_Startall(4, receive_requests_2);
			}
			error = 0.0; // Reset calculated error for each calcuation
		}

		/***************** Start Internal elements calculation *****************/

#ifdef DEBUG
		// if(world_rank == 0)
		// {
		// printf("Internal elements calculation\n");
		// }
#endif
		// First nested iteration to calculate internal elements
		// row 2 to row m-1
#pragma omp for reduction(+ : error) private(x, y, fY, fX) schedule(static, 1) collapse(2)
		for(y = 2; y < m; y++)
		{
			// fY = yBottom + (y - 1) * deltaY;
			// Column 2 to column n-1
			for(x = 2; x < n; x++)
			{
#ifdef DEBUG
				// if(world_rank == 0)
				// {
				// printf("(%d,%d)\n", y, x);
				// }
#endif
				fY = yUp - (y - 1) * deltaY;
				fX = xLeft + (x - 1) * deltaX;
				f = -alpha * (1.0 - fX * fX) * (1.0 - fY * fY) - 2.0 * (1.0 - fX * fX)
					- 2.0 * (1.0 - fY * fY);
				updateVal = ((SRC(x - 1, y) + SRC(x + 1, y)) * cx
							 + (SRC(x, y - 1) + SRC(x, y + 1)) * cy + SRC(x, y) * cc - f)
							/ cc;
				DST(x, y) = SRC(x, y) - relax * updateVal;
				error += updateVal * updateVal;
			}
		}
		/***************** End Internal elements calculation *****************/

		/***************** Start wait for MPI messages  *****************/
#pragma omp master
		// One thread waits for all MPI requests to finish
		{
			if(iterationCount % 2 == 0)
			{
				MPI_Waitall(4, receive_requests_1, receive_statuses_1); // Wait to receive data
			}
			else
			{
				MPI_Waitall(4, receive_requests_2, receive_statuses_2); // Wait to receive data
			}
		}
#pragma omp barrier

		/***************** End wait for MPI messages  *****************/

		/***************** Start External elements calculation *****************/

#ifdef DEBUG
		// if(world_rank == 0)
		// {
		// printf("Rows 1 & %d\n", m);
		// }
#endif
		// row 1 & row m
#pragma omp for reduction(+ : error) private(x, y, fX, fY) schedule(static, 1) collapse(2)
		for(y = 1; y < m + 1; y = y + m - 1)
		{
			// fY = yBottom + (y - 1) * deltaY;
			// Column 1 to column n
			for(x = 1; x < n + 1; x++)
			{
#ifdef DEBUG
				// if(world_rank == 0)
				// {
				// 	printf("(%d,%d)\n", y, x);
				// }
#endif
				fY = yUp - (y - 1) * deltaY;
				fX = xLeft + (x - 1) * deltaX;
				f = -alpha * (1.0 - fX * fX) * (1.0 - fY * fY) - 2.0 * (1.0 - fX * fX)
					- 2.0 * (1.0 - fY * fY);
				updateVal = ((SRC(x - 1, y) + SRC(x + 1, y)) * cx
							 + (SRC(x, y - 1) + SRC(x, y + 1)) * cy + SRC(x, y) * cc - f)
							/ cc;
				DST(x, y) = SRC(x, y) - relax * updateVal;
				error += updateVal * updateVal;
			}
		}

#ifdef DEBUG
		// if(world_rank == 0)
		// {
		// printf("Column 1\n");
		// }
#endif
		// column 1
#pragma omp for reduction(+ : error) private(x, y, fX, fY) schedule(static, 1) collapse(2)
		for(y = 2; y < m; y++)
		{
			for(x = 1; x < 2; x++) // Unecessary for, it's only one iteration
			{
#ifdef DEBUG
				// if(world_rank == 0)
				// {
				// printf("(%d,%d)\n", y, x);
				// }
#endif
				fY = yUp - (y - 1) * deltaY;
				fX = xLeft + (x - 1) * deltaX;
				f = -alpha * (1.0 - fX * fX) * (1.0 - fY * fY) - 2.0 * (1.0 - fX * fX)
					- 2.0 * (1.0 - fY * fY);
				updateVal = ((SRC(x - 1, y) + SRC(x + 1, y)) * cx
							 + (SRC(x, y - 1) + SRC(x, y + 1)) * cy + SRC(x, y) * cc - f)
							/ cc;
				DST(x, y) = SRC(x, y) - relax * updateVal;
				error += updateVal * updateVal;
			}
		}

#ifdef DEBUG
		// if(world_rank == 0)
		// {
		// 	printf("Column %d\n", n);
		// }
#endif
		// column n
#pragma omp for reduction(+ : error) private(x, y, fX, fY) schedule(static, 1) collapse(2)
		for(y = 2; y < m; y++)
		{
			for(x = n; x < n + 1; x++) // Unecessary for, it's only one iteration
			{
#ifdef DEBUG
				// if(world_rank == 0)
				// {
				// 	printf("(%d,%d)\n", y, x);
				// }
#endif
				fY = yUp - (y - 1) * deltaY;
				fX = xLeft + (x - 1) * deltaX;
				f = -alpha * (1.0 - fX * fX) * (1.0 - fY * fY) - 2.0 * (1.0 - fX * fX)
					- 2.0 * (1.0 - fY * fY);
				updateVal = ((SRC(x - 1, y) + SRC(x + 1, y)) * cx
							 + (SRC(x, y - 1) + SRC(x, y + 1)) * cy + SRC(x, y) * cc - f)
							/ cc;
				DST(x, y) = SRC(x, y) - relax * updateVal;
				error += updateVal * updateVal;
			}
		}

		/***************** End External elements calculation *****************/
		// printf(
		// 	"Thread %d, iteration %d, error = %f\n", omp_get_thread_num(), iterationCount, error);
#pragma omp master
		local_error = error;

		/********** End inlined one_jacobi_iteration *********/
#pragma omp master
		{
			if(iterationCount % 2 == 0)
			{
				MPI_Waitall(4, send_requests_1, send_statuses_1); // Wait to send data
			}
			else
			{
				MPI_Waitall(4, send_requests_2, send_statuses_2); // Wait to send data
			}
		}

		iterationCount++;

#pragma omp barrier
#pragma omp master
		{
			// Swap the buffers
			tmp = u_old;
			u_old = u;
			u = tmp;

			// Sum each process' error in process 0

			MPI_Reduce(&local_error, &total_error, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

			// Process 0 calculates final error
			if(world_rank == 0)
			{
				total_error = sqrt(total_error) / (n * m);
			}

			// Bcast total error back to processes
			// so that they know if they should stop or not
			MPI_Bcast(&total_error, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
		}
#pragma omp barrier
		// printf("mpirank=%2d Trank=%2d (iteration=%2d)\n",
		// 	   world_rank,
		// 	   omp_get_thread_num(),
		// 	   iterationCount);
	}

	/************ End jacobi calculation ************/

	t2 = MPI_Wtime();
	if(world_rank == 0)
	{
		printf("Iterations=%3d Elapsed MPI Wall time is %f\n", iterationCount, t2 - t1);
	}

	/************ Start Parallel I/O ************/

	// printf("Process %d bottom-right coord: %d, %d\n", world_rank, );
#ifdef SAVE_TO_FILE
	// Create/Open file where each process' final u_old data will be stored in
	MPI_File fh;
	MPI_File_open(
		MPI_COMM_WORLD, "matrix.txt", MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);

#define STRING_SIZE 30
	char header[STRING_SIZE];		   // Final string which will be written to file
	char header_temp[STRING_SIZE - 1]; // A temporary buffer for storing initial string
	char format_string[8];

	// Buffer where all the data to be written will be stored in.
	// Instead of writing line by line, it's suggested that MPI does
	// a small number of contiguous file accesses per process
	char *final_string = (char *)calloc((STRING_SIZE - 1) + 1, sizeof(char));

	// The file offset that each process will start writing at
	MPI_Offset offset = (world_rank * n * m * (STRING_SIZE - 1));
	MPI_Offset last_str_offset = offset; // Remember last line's write offset

	sprintf(format_string, "%%%ds\n", STRING_SIZE - 2); // Calculate format string

	MPI_Barrier(MPI_COMM_WORLD);
	MPI_File_set_view(fh, last_str_offset, MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
	t1 = MPI_Wtime(); // Parallel start time
	// Iterate u_old and store to file
	for(int i = 1; i < m + 1; i++)
	{
		// final_string[0] = '\0';

		for(int j = 1; j < n + 1; j++)
		{
			// Initial format for line to be written.
			// Stores X coordinate, Y coordinate and respective Z value
			sprintf(header_temp,
					"%f,%f,%f",
					-1 + ((j + (coords[1] * n)) - 1) * deltaX, // X coordinate
					1 - ((i + (coords[0] * m)) - 1) * deltaY,  // Y coordinate
					SRC(j, i));								   // Z value

			// Copy to final string, padding with spaces for predictable string size
			snprintf(header, STRING_SIZE, format_string, header_temp);
			// strcat(final_string, header);
			// Add string size offset to previous offset, ready to write next line
			MPI_File_write_all(fh, header, (((STRING_SIZE - 1))), MPI_CHAR, MPI_STATUS_IGNORE);
			last_str_offset = last_str_offset + (STRING_SIZE - 1);
		}
		// Write line in file at correct file location
	}

	MPI_File_close(&fh);

	t2 = MPI_Wtime();
	if(world_rank == 0)
	{
		printf("Elapsed MPI Parallel I/O time is %f\n", t2 - t1);
	}

#endif
	/************ End Parallel I/O ************/

	// TODO -- Don't know if this should be ran at all
	if(world_rank == 0)
	{
		diff = clock() - start;
		int msec = diff * 1000 / CLOCKS_PER_SEC;
		printf("Time taken %d seconds %d milliseconds\n", msec / 1000, msec % 1000);
		printf("Residual %g\n", total_error);

		// 	// u_old holds the solution after the most recent buffers swap
		// 	double absoluteError
		// 		= checkSolution(xLeft, yBottom, n + 2, m + 2, u_old, deltaX, deltaY, alpha);

		// 	printf("The error of the iterative solution is %g\n", absoluteError);
	}

	/************ Cleanup ****************/
	free(u);
	free(u_old);
#ifdef SAVE_TO_FILE
	free(final_string);
#endif
	MPI_Request_free(&receive_requests_1[NORTH]);
	MPI_Request_free(&receive_requests_1[SOUTH]);
	MPI_Request_free(&receive_requests_1[EAST]);
	MPI_Request_free(&receive_requests_1[WEST]);
	MPI_Request_free(&receive_requests_2[NORTH]);
	MPI_Request_free(&receive_requests_2[SOUTH]);
	MPI_Request_free(&receive_requests_2[EAST]);
	MPI_Request_free(&receive_requests_2[WEST]);
	MPI_Request_free(&send_requests_1[NORTH]);
	MPI_Request_free(&send_requests_1[SOUTH]);
	MPI_Request_free(&send_requests_1[EAST]);
	MPI_Request_free(&send_requests_1[WEST]);
	MPI_Request_free(&send_requests_2[NORTH]);
	MPI_Request_free(&send_requests_2[SOUTH]);
	MPI_Request_free(&send_requests_2[EAST]);
	MPI_Request_free(&send_requests_2[WEST]);
	MPI_Type_free(&column_type);
	MPI_Finalize();

	return 0;
}
