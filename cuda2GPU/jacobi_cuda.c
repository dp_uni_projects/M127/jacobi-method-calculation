#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <omp.h>

// #define SAVE_TO_FILE

/**********************************************************
 * Checks the error between numerical and exact solutions
 **********************************************************/
double checkSolution(double	 xStart,
					 double	 yStart,
					 int	 maxXCount,
					 int	 maxYCount,
					 double *u,
					 double	 deltaX,
					 double	 deltaY,
					 double	 alpha)
{
#define U(XX, YY) u[(YY)*maxXCount + (XX)]
	int	   x, y;
	double fX, fY;
	double localError, error = 0.0;

	for(y = 1; y < (maxYCount - 1); y++)
	{
		fY = yStart + (y - 1) * deltaY;
		for(x = 1; x < (maxXCount - 1); x++)
		{
			fX = xStart + (x - 1) * deltaX;
			localError = U(x, y) - (1.0 - fX * fX) * (1.0 - fY * fY);
			error += localError * localError;
		}
	}
	return sqrt(error) / ((maxXCount - 2) * (maxYCount - 2));
}

extern "C" float
jacobiGPU(double *, double *, int, int, double, double, double, int, int, double *, double *);

int main(int argc, char **argv)
{
	int	   n, m, mits;
	double alpha, tol, relax;
	// double maxAcceptableError;

	double *u, *u_old, *tmp;
	double	t1, t2;

	//    printf("Input n,m - grid dimension in x,y direction:\n");
	scanf("%d,%d", &n, &m);
	//    printf("Input alpha - Helmholtz constant:\n");
	scanf("%lf", &alpha);
	//    printf("Input relax - successive over-relaxation parameter:\n");
	scanf("%lf", &relax);
	//    printf("Input tol - error tolerance for the iterrative solver:\n");
	scanf("%lf", &tol);
	//    printf("Input mits - maximum solver iterations:\n");
	scanf("%d", &mits);

	printf("-> %d, %d, %g, %g, %g, %d\n", n, m, alpha, relax, tol, mits);

	size_t freeCUDAMem, totalCUDAMem;
	cudaMemGetInfo(&freeCUDAMem, &totalCUDAMem);
	printf("Total GPU memory %zu, free %zu\n", totalCUDAMem, freeCUDAMem);

	int allocCount = (n + 2) * (m + 2);
	// Those two calls also zero the boundary elements
	u = (double *)calloc(allocCount, sizeof(double)); // reverse order
	u_old = (double *)calloc(allocCount, sizeof(double));

	// Shared memory for halo exchange between GPUs
	double *	shared_halo;
	cudaError_t err;
	// shared_halo = (double *)calloc(2 * (n + 2), sizeof(double));
	shared_halo = (double *)malloc(2 * (n + 2) * sizeof(double));
	// cudaMallocManaged((void **)&shared_halo, 2 * (n + 2) * sizeof(double));
	for(int i = 0; i < 2 * (n + 2); i++)
	{
		shared_halo[i] = 0.0;
	}

	double *h_total_error;
	h_total_error = (double *)malloc(2 * sizeof(double)); // one for each thread
	h_total_error[0] = HUGE_VAL;
	h_total_error[1] = HUGE_VAL;

	printf("allocCount(CPU)=%d (%5d rows x %5d cols)\n", allocCount, n + 2, m + 2);

	printf("sharedHalo size=2 rows x %d cols \n", (n + 2));

	if(u == NULL || u_old == NULL)
	{
		printf("Not enough memory for two %ix%i matrices\n", n + 2, m + 2);
		exit(1);
	}

	omp_set_num_threads(2);
#pragma omp parallel
	{
		int TotalGpuNum;
		int ID = omp_get_thread_num();
		int numthreads = omp_get_max_threads();
		int pointer_offset_read = ID * (n + 2) * (m / 2);
		int pointer_offset_write = ID * (n + 2) * ((m / 2) + 1);
		printf("Thread %d: u offset read = %d, u_old offset write = %d\n",
			   ID,
			   pointer_offset_read,
			   pointer_offset_write);
		jacobiGPU(u + pointer_offset_read,
				  u_old + pointer_offset_write,
				  n,
				  m / numthreads,
				  alpha,
				  relax,
				  tol,
				  mits,
				  ID,
				  shared_halo,
				  h_total_error);
	}

	// diff = clock() - start;
	// int msec = diff * 1000 / CLOCKS_PER_SEC;
	// printf("Time taken %d seconds %d milliseconds\n", msec / 1000, msec % 1000);
	// printf("Residual %g\n", error);

	// u_old holds the solution after the most recent buffers swap
	//	double absoluteError
	//	= checkSolution(xLeft, yBottom, n + 2, m + 2, u_old, deltaX, deltaY, alpha);
	// printf("The error of the iterative solution is %g\n", absoluteError);

#ifdef SAVE_TO_FILE
	double xLeft = -1.0, xRight = 1.0;
	double yBottom = -1.0, yUp = 1.0;

	double deltaX = (1.0 - (-1.0)) / (n - 1);
	double deltaY = (1.0 - (-1.0)) / (m - 1);
#define SRC(XX, YY) u_old[(YY) * (n + 2) + (XX)] // prosbash ana grammh, aplo
	FILE *fp = fopen("matrix.txt", "w+");
	for(int i = 1; i < m + 1; i++)
	{

		for(int j = 1; j < n + 1; j++)
		{
			// Initial format for line to be written.
			// Stores X coordinate, Y coordinate and respective Z value
			fprintf(fp,
					"%f,%f,%f\n",
					-1 + (j - 1) * deltaX, // X coordinate
					1 - (i - 1) * deltaY,  // Y coordinate
					SRC(j, i));			   // Z value
		}
		// Write line in file at correct file location
	}
#endif

	free(u);
	free(u_old);
	free(h_total_error);
	return 0;
}
