#include <lcutil.h>
#include <timestamp.h>

#define BLOCK_SIZE 512

__global__ void
calc(double *u, double *u_old, int n, int m, double alpha, double relax, double tol, int mits, double* error_array_device)
{

  // Solve in [-1, 1] x [-1, 1]
  double xLeft = -1.0, xRight = 1.0;
  double yBottom = -1.0, yUp = 1.0;

  double deltaX = (1.0 - (-1.0)) / (n - 1);
  double deltaY = (1.0 - (-1.0)) / (m - 1);

  int iterationCount = 0;

#define SRC(XX, YY) u_old[(YY) * (n + 2) + (XX)] // prosbash ana grammh, aplo
#define DST(XX, YY) u[(YY) * (n + 2) + (XX)]
  int	   x, y;
  double fX, fY;

  double updateVal;
  //  double f;
  // Coefficients
  double cx = 1.0 / (deltaX * deltaX);
  double cy = 1.0 / (deltaY * deltaY);
  double cc = -2.0 * cx - 2.0 * cy - alpha;
  // double			  local_error = 0.0;
  // __shared__ double total_error = HUGE_VAL; // to total error to grafoun ola mazi
  // __shared__ double error = 0.0;

  int blockId = (gridDim.x * blockIdx.y) + blockIdx.x;
  int threadId = (blockId * blockDim.x) + threadIdx.x;

  // briskw se poio block eimai orizontia * posa threads exei kathe block = poses sthles exw
  // aristera mou + poio thread eimai sthn orizontai diata3h tou block
  x = (blockIdx.x * blockDim.x + threadIdx.x) + 1;
  y = blockIdx.y + 1;

  if(x < (n + 2 - 1)) // if current thread is within the array
    {
      int array_write_idx = (y-1) * n + (x-1);
      //printf("Thread %5d: Write at %5d, Coords: %5d, %5d\n", threadId, array_write_idx,x, y);      
      //      error_array_device[threadId] = 0.0;      
      fY = yUp - (y - 1) * deltaY;
      updateVal = ((SRC(x - 1, y) + SRC(x + 1, y)) * cx + (SRC(x, y - 1) + SRC(x, y + 1)) * cy
		   + SRC(x, y) * cc
		   - (-alpha * (1.0 - (xLeft + (x - 1) * deltaX) * (xLeft + (x - 1) * deltaX))
		      * (1.0 - fY * fY)
		      - 2.0 * (1.0 - (xLeft + (x - 1) * deltaX) * (xLeft + (x - 1) * deltaX))
		      - 2.0 * (1.0 - fY * fY)))
	/ cc;
      DST(x, y) = SRC(x, y) - relax * updateVal;

      error_array_device[array_write_idx] = updateVal * updateVal;
      
      // error += updateVal * updateVal;
      // Store aggregate error for each block in different array location

      // error += local_error; // athroizoun to local error gia na doun an prepei na sunexisoun
      // gia allh epanalhpsh

      // total_error = sqrt(error) / (n * m); // upologizoun total error afou exoun kanei ola ta
      // thread add iterationCount++;
  }
  //__syncthreads();
}

__global__ void reduce_sum(double *array, int array_size) {
  int idx = threadIdx.x;
  double sum = 0;
  for (int i = idx; i < array_size; i += blockDim.x)
    {
      sum += array[i];
    }
  __shared__ double r[1024];
  r[idx] = sum; // Store current thread's local sum
  __syncthreads();
  for (int size = blockDim.x/2; size>0; size/=2) //uniform
    { 
      if (idx<size)
	{
	  r[idx] += r[idx+size];
	}
      __syncthreads();
    }
  if (idx == 0)
    {
      array[0] = r[0];
    }
}

extern "C" float jacobiGPU(
	double *h_u, double *h_u_old, int n, int m, double alpha, double relax, double tol, int mits)
{
	cudaError_t err;
	int			allocCount = (n + 2) * (m + 2);
	double *	d_u;
	double *	d_u_old;

	err = cudaMalloc((void **)&d_u, allocCount * sizeof(double));
	if(err != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s\n", err);
		return err;
	}

	err = cudaMalloc((void **)&d_u_old, allocCount * sizeof(double));
	if(err != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s\n", err);
		return err;
	}

	// Copy data to device memory
	err = cudaMemcpy(d_u, h_u, sizeof(double) * allocCount, cudaMemcpyHostToDevice);
	if(err != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s\n", err);
		return err;
	}

	err = cudaMemcpy(d_u_old,
					 h_u_old,
					 sizeof(double) * allocCount,
					 cudaMemcpyHostToDevice); // pithanh kathusterhsh

	if(err != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s\n", err);
		return err;
	}

	timestamp t_start;
	t_start = getTimestamp();

	dim3 dimBl(BLOCK_SIZE); // 1D block

	// sto x exw n+2 sthles, ston y exw n+2 grammes, ara orizontai kanw
	// stoixeia/threads, katheta kanw tosa block osa stoixeia exw, ta threads ta
	// pairnw ana grammh
	dim3 dimGr(FRACTION_CEILING(n, BLOCK_SIZE), m);

	int		iterationCount = 0;
	double *error_array_device;
	int		error_array_size = (n * m);
	err = cudaMallocManaged(&error_array_device, sizeof(double) * error_array_size);
	if(err != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s\n", err);
		return err;
	}

	error_array_device[0] = HUGE_VAL;
	while(iterationCount < mits && error_array_device[0] > tol)
	{
		// kernel call
		calc<<<dimGr, dimBl>>>(d_u, d_u_old, n, m, alpha, relax, tol, mits, error_array_device);
		err = cudaDeviceSynchronize(); // sygxronismos anamonh na teleiwsoun oles oi douleies
		if(err != cudaSuccess)
		{
			fprintf(stderr, "GPUassert: %s\n", err);
			return err;
		}
		// Call reduction kernel
		reduce_sum<<<1, 1024>>>(error_array_device, error_array_size);
		err = cudaDeviceSynchronize(); // sygxronismos anamonh na teleiwsoun oles oi douleies
		if(err != cudaSuccess)
		{
			fprintf(stderr, "GPUassert: %s\n", err);
			return err;
		}

		double *temp = d_u_old;
		d_u_old = d_u;
		d_u = temp;
		iterationCount++;
	}
	printf("Error %g\n", error_array_device[0]);
	printf("Run %d iterations\n", iterationCount);

	err = cudaGetLastError();
	if(err != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s\n", err);
		return err;
	}

	err = cudaDeviceSynchronize(); // sygxronismos anamonh na teleiwsoun oles oi douleies
	if(err != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s\n", err);
		return err;
	}

	float msecs = getElapsedtime(t_start);
	printf("Run time %.5f s\n", msecs / 1000);
	// Copy results back to host memory
	err = cudaMemcpy(h_u_old, d_u_old, sizeof(double) * allocCount, cudaMemcpyDeviceToHost);
	if(err != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s\n", err);
		return err;
	}

	// Free memory
	err = cudaFree(d_u);
	if(err != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s\n", err);
		return err;
	}
	err = cudaFree(d_u_old);
	if(err != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s\n", err);
		return err;
	}

	return msecs;
}
