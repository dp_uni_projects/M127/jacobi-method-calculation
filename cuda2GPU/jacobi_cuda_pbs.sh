#!/bin/bash

# JobName #
#PBS -N jacobi_cuda

#Which Queue to use #
#PBS -q GPUq

# Only this job uses the chosen nodes
#PBS -l place=excl

# Stdout Output File, if not provided a <JobName>.o<JobID> will be created #
#PBS -o ./results/jacobi_cuda.out

# Stderr Output File, if not provided a <JobName>.e<JobID> will be created #
#PBS -e ./results/jacobi_cuda.err

# Max Wall time, Example 1 Minute #
#PBS -l walltime=00:20:00

# How many nodes and tasks per node
#PBS -l select=1:ncpus=10:ompthreads=20:ngpus=2

#Change Working directory to SUBMIT directory
cd $PBS_O_WORKDIR

# Run executable #
#nvprof --unified-memory-profiling off ./bin/jacobi_cuda.x < input
nvprof --print-summary-per-gpu --unified-memory-profiling off ./bin/jacobi_cuda_2gpus.x < input
