gSHELL:=/bin/bash
CUDA_INSTALL_PATH = /usr/local/cuda-11.1
CC = g++
OPTFLAG = -O2 -fomit-frame-pointer -ftree-vectorize -ftree-vectorizer-verbose=0  -funroll-loops -fopenmp
NVCC = ${CUDA_INSTALL_PATH}/bin/nvcc
INCDIR = -Icuda/common/inc/
FLAGS = ${OPTFLAG} -I${CUDA_INSTALL_PATH}/include -Wall -g ${INCDIR} -fopenmp
NVFLAGS = -O2 -I${CUDA_INSTALL_PATH}/include --compiler-options -fno-strict-aliasing --ptxas-options=-v -g ${INCDIR} -arch=sm_61
BITS = $(shell getconf LONG_BIT)
ifeq (${BITS},64)
        LIBSUFFIX := 64
endif
LFLAGS = -L${CUDA_INSTALL_PATH}/lib${LIBSUFFIX} -lm -lstdc++ -lcudart -fopenmp
#CLEAN_FILES = simple_add1GPUOmp gpu_add.o simple_add1GPUOmp.o cpu_add.o


# Serial
jacobi_serial:
	# module load openmpi4
	# module load mpip
	mpicc -O3 serial/jacobi_serial.c -o bin/jacobi_serial.x -lm
	mpicc -O3 -g serial/jacobi_serial.c -L$$MPIP_DIR/lib -lmpiP -lm -lbfd -lunwind -o bin/jacobi_serial_mpip.x

# MPI
jacobi_mpi:
	# module load openmpi4
	# module load mpip
	mpicc -O3 mpi/jacobi_mpi.c -o bin/jacobi_mpi.x -lm
	mpicc -O3 -g mpi/jacobi_mpi.c -L$$MPIP_DIR/lib -lmpiP -lbfd -lm -lunwind -o bin/jacobi_mpi_mpip.x

# Hybrid openMP + MPI
jacobi_hybrid: 
# 	module load openmpi4
# 	module load mpip
	mpicc -O3 -fopenmp -lm hybrid/jacobi_hybrid.c -o bin/jacobi_hybrid.x
	mpicc -O3 -g -fopenmp hybrid/jacobi_hybrid.c -L$$MPIP_DIR/lib -lmpiP -lbfd -lunwind -lm -o bin/jacobi_hybrid_mpip.x


jacobi_cuda: jacobi_cuda.o gpu_calc.o
	${CC} ${LFLAGS} bin/jacobi_cuda.o bin/gpu_calc.o -o bin/jacobi_cuda.x

jacobi_cuda.o:
	${CC} -c ${FLAGS} cuda2GPU/jacobi_cuda.c -o bin/jacobi_cuda.o

gpu_calc.o: 
	${NVCC} ${NVFLAGS} -DUNIX -c cuda2GPU/gpu_calc.cu -o bin/gpu_calc.o -Xcompiler -fopenmp


jacobi_cuda_2gpus: jacobi_cuda_2gpus.o gpu_calc_2gpus.o
	${CC} ${LFLAGS} bin/jacobi_cuda_2gpus.o bin/gpu_calc_2gpus.o -o bin/jacobi_cuda_2gpus.x

jacobi_cuda_2gpus.o:
	${CC} -c ${FLAGS} cuda2GPU/jacobi_cuda.c -o bin/jacobi_cuda_2gpus.o

gpu_calc_2gpus.o: 
	${NVCC} ${NVFLAGS} -DUNIX -c cuda2GPU/gpu_calc.cu -o bin/gpu_calc_2gpus.o -Xcompiler -fopenmp

clean:
	rm -f bin/*.x				# executables
	rm -f bin/*.o				# object files
	rm -f *.e*					# stderr
	rm -f *.o*					# stdout

all: clean serial mpi


